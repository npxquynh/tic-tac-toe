# Start the game

```
rake game:start
```

Playing between me and the _Smarty Bot_. [See here - from _asciinema_](https://asciinema.org/a/VnAwcfs2XbfGBp6JMrcIVz1mL)

Example of 2 _Smarty Bot_ playing against each other. With `MAX_DEPTH = 5` so that the decision can be made faster. [See here - from _asciinema_](https://asciinema.org/a/REPBS2P8jqUEi5R7lemfuXUKv)

Example of 2 _Smarty Bot_ playing against each other. With `MAX_DEPTH = 8`. You'll notice that the speed is significantly slower because the search space is huge when the board is empty. [See here - from _asciinema_](https://asciinema.org/a/hSwWIqPWqRSb1GUwS003kHZEK)

# Run the test

```
rake test
```
