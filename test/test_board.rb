require 'test/unit'
require 'app'

class TestBoard < Test::Unit::TestCase
  def test_empty_after_initializing()
    board = Board.new

    (0...3).each do |x|
      (0...3).each do |y|
        assert_equal(true, board.empty?(x, y))
      end
    end
  end

  def test_tick_sign_for_empty_cell()
    board = Board.new

    result = board.tick('X', 0, 0)
    assert(result.eql?('X'), 'Empty cell should have new value')
  end

  def test_tick_sign_for_non_empty_cell()
    board = Board.new
    board.tick('X', 0, 0)

    assert_raise Board::CellNotAvailableForTickingError do
      board.tick('O', 0, 0)
    end
  end

  def test_tick_non_supported_sign()
    board = Board.new

    assert_raise Board::SignNotSupportedError do
      board.tick('A', 0, 0)
    end
  end

  def test_tick_sign_for_index_outside_of_board()
    board = Board.new

    assert_raise Board::CellNotAvailableForTickingError do
      board.tick('O', 100, 100)
    end
  end

  def test_empty_cell_indices()
    board = Board.create_simple_board('012010012')

    result = board.empty_cell_indices
    assert_equal([[0, 0], [1, 0], [1, 2], [2, 0]], result)
  end

  def test_winner_when_there_is_no_winner()
    # oox
    # __x
    # ___
    board = Board.create_simple_board('221001000')

    result = board.winner?
    assert(result.eql?(false), 'should be false')
  end

  def test_winner_when_there_is_a_winner()
    # ooo
    # __x
    # ___
    board = Board.create_simple_board('222001000')

    result = board.winner?
    assert(result.eql?(true), 'should be true')
  end

  def test_find_winner_horizontal()
    # ooo
    # __x
    # ___
    board = Board.create_simple_board('222001000')

    result = board.find_winner
    assert(result.eql?('O'), 'Winner should be found')
  end

  def test_find_winner_vertical()
    # oox
    # o_x
    # __x
    board = Board.create_simple_board('221201001')

    result = board.find_winner
    assert(result.eql?('X'), 'Winner should be found')
  end

  def test_find_winner_diagonal
    # xox
    # ox_
    # xoo
    board = Board.create_simple_board('121210122')

    result = board.find_winner
    assert(result.eql?('X'), 'Winner should be found')
  end

  def test_find_winner_no_winner()
    board = Board.new
    board.tick('X', 0, 2)
    board.tick('X', 1, 2)

    result = board.find_winner
    assert(result.eql?(Board::NO_WINNER), 'Winner should not be found')
  end


  def test_create_simple_board_and_printing
    board = Board.create_simple_board('102021210')

    expected_output = <<~OUTPUT
        0 1 2
      0|X| |O|
      1| |O|X|
      2|O|X| |
    OUTPUT

    assert_equal(expected_output, board.to_s)
  end
end
