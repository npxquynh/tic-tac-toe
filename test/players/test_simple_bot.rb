require 'test/unit'
require 'app'

class TestPlayersSimpleBot < Test::Unit::TestCase
  def test_select_cell()
    board = Board.new
    selected_cell = Players::SimpleBot.new(board.signs.first).select_cell(board)

    assert_equal(selected_cell, [0, 0])
  end

  def test_select_the_first_empty_cell()
    board = Board.new(%w(A B))
    board.tick('A', 0, 0)

    selected_cell = Players::SimpleBot.new(board.signs.first).select_cell(board)
    assert_equal(selected_cell, [0, 1])
  end
end
