require 'test/unit'
require 'app'

class MockStream
  attr_reader :output

  def initialize(input = [])
    @input = input
    @output = []
  end

  def readline
    @input.shift
  end

  def write(output)
    @output << output
  end
end

class TestBoard < Test::Unit::TestCase
  def test_select_cell_with_stream()
    board = Board.new

    input_stream = MockStream.new(["0 0\n"])
    output_stream = MockStream.new
    selected_cell = Players::CommandLine.new(board.signs.first).select_cell_with_stream(board, input_stream, output_stream)

    assert_equal([0, 0], selected_cell)
  end

  def test_select_non_empty_cell_prints_out_error_message()
    board = Board.new
    board.tick('X', 0, 0)

    input_stream = MockStream.new(["0 0\n", "1 2\n"])
    output_stream = MockStream.new
    selected_cell = Players::CommandLine.new(board.signs.first).select_cell_with_stream(board, input_stream, output_stream)

    assert_equal("Please choose another cell to tick. The one you selected is not empty.\n", output_stream.output.last)
    assert_equal([1, 2], selected_cell)
  end
end
