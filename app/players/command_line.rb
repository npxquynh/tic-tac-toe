module Players
  class CommandLine < Player
    INPUT_SEPARATOR = ' '

    INSTRUCTION = "Enter the position of cell you would like to tick. For example '0 0' (the top left corner) and then press Enter.\n"
    INSTRUCTION_FOR_WRONG_SELECTING = "Please choose another cell to tick. The one you selected is not empty.\n"

    def select_cell(board)
      select_cell_with_stream(board, App::INPUT_STREAM, App::OUTPUT_STREAM)
    end

    def select_cell_with_stream(board, input_stream, output_stream)
      output_stream.write(INSTRUCTION)

      loop do
        input = input_stream.readline()
        selected_cell = input.strip.split(INPUT_SEPARATOR).map(&:to_i)
        return selected_cell if board.empty?(*selected_cell)

        output_stream.write(INSTRUCTION_FOR_WRONG_SELECTING)
      end

      return selected_cell
    end
  end
end
