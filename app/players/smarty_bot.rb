module Players
  class SmartyBot < Player
    MAX_DEPTH = 5

    def select_cell(board)
      minimax(board, @sign)[:coordinate]
    end

    def minimax(board, current_sign, depth = 0)
      return {score: 0} if depth == MAX_DEPTH

      other_sign = (board.signs - [current_sign]).first

      winner = board.find_winner
      if (@sign == winner)
        return {score: 10}
      elsif ((board.signs - [@sign]).first == winner)
        return {score: -10}
      elsif board.full?
        return {score: 0}
      end

      moves = []

      board.empty_cell_indices.each do |coordinate|
        move = {}
        move[:coordinate] = coordinate

        board.tick(current_sign, coordinate.first, coordinate.last)

        result = minimax(board, other_sign, depth + 1)
        move[:score] = result[:score]

        board.untick(coordinate.first, coordinate.last)

        moves << move
      end

      if @sign == current_sign
        best_score, best_move = *(sorted_with_index(moves).last) # largest score
      else
        best_score, best_move = *(sorted_with_index(moves).first) # smallest score
      end

      return moves[best_move]
    end

    def sorted_with_index(moves)
      moves.map.with_index.sort_by { |elem, index| [elem[:score], index] }
    end
  end
end
