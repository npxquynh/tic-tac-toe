module Players
  class SimpleBot < Player
    def select_cell(board)
      (0...Board::SIZE).each do |x|
        (0...Board::SIZE).each do |y|
          return [x, y] if board.empty?(x, y)
        end
      end
      return []
    end
  end
end
