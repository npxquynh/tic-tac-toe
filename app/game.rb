class Game
  def initialize
    @board = Board.new
    @players = [
      Players::CommandLine.new(@board.signs.first),
      Players::SmartyBot.new(@board.signs.last)
    ]
    @current_player_index = 0
  end

  def start
    while (!@board.full? && !@board.winner?)
      @board.display

      selected_cell = current_player.select_cell(@board)
      print_selected_cell(selected_cell)

      @board.tick(current_player.sign, selected_cell.first, selected_cell.last)
      swap_player
    end

    @board.display

    if @board.winner?
      puts "Winner is #{@board.find_winner}"
    end

    if @board.full?
      puts "Board is full, and it is a draw"
    end
  end

  private

  def print_selected_cell(selected_cell)
    puts "Player #{current_player.sign} picks #{selected_cell}"
  end

  def swap_player
    @current_player_index = @current_player_index == 0 ? 1 : 0
  end

  def current_player
    @players[@current_player_index]
  end
end
