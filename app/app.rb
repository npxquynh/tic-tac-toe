$LOAD_PATH.unshift File.expand_path('../../app', __FILE__)

require 'board'
require 'player'
require 'players/command_line'
require 'players/simple_bot'
require 'players/smarty_bot'
require 'game'

module App
  INPUT_STREAM = STDIN
  OUTPUT_STREAM = STDOUT
end
