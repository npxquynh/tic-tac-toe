class Board
  EMPTY = ' '
  SIGNS = %w(X O)
  CELL_SEPARATOR = '|'
  SIZE = 3
  WINNING_COUNT = 3
  NO_WINNER = 'no_winner'

  InvalidInputError = Class.new(StandardError)
  SignNotSupportedError = Class.new(StandardError)
  CellNotAvailableForTickingError = Class.new(StandardError)

  attr_reader :signs

  def initialize(signs = SIGNS)
    raise InvalidInputError, 'signs should have 2 elements. Ex: ["X", "O"]' unless signs.size == 2

    @signs = signs
    @cells = Array.new(SIZE) { Array.new(SIZE, EMPTY) }
    @winner = NO_WINNER
  end

  def self.create_simple_board(values)
    elements_count = SIZE * SIZE

    raise StandardError, "values need to have #{elements_count} characters from [0, 1, 2]" unless values =~ /[012]{#{Regexp.escape(elements_count.to_s)}}/

    board = new
    (0...elements_count).each do |index|
      case values[index]
      when '1'
        board.tick(board.signs.first, index / SIZE, index % SIZE)
      when '2'
        board.tick(board.signs.last, index / SIZE, index % SIZE)
      end
    end

    board
  end

  def tick(sign, x, y)
    raise SignNotSupportedError unless supported?(sign)
    raise CellNotAvailableForTickingError unless empty?(x, y)

    @cells[x][y] = sign
  end

  def untick(x, y)
    raise StandardError, 'cell is currently empty' if empty?(x, y)
    @cells[x][y] = EMPTY
  end

  def valid_cell_position?(x, y)
    0 <= x && x < SIZE && 0 <= y && y < SIZE
  end

  def empty?(x, y)
    return false unless valid_cell_position?(x, y)

    @cells[x][y] == EMPTY
  end

  def full?
    (0...SIZE).each do |x|
      (0...SIZE).each do |y|
        return false if @cells[x][y] == EMPTY
      end
    end

    true
  end

  def empty_cell_indices
    indices = (0...SIZE).flat_map do |x|
      (0...SIZE).map do |y|
        [x, y] if empty?(x, y)
      end
    end

    indices.compact
  end

  def winner?
    find_winner != NO_WINNER
  end

  def find_winner
    winner = NO_WINNER

    (0...SIZE).each do |x|
      (0...SIZE).each do |y|
        next if empty?(x, y)

        winner = find_winner_at_cell(x, y)
        return winner if winner != NO_WINNER
      end
    end

    NO_WINNER
  end

  def display
    system 'clear'
    puts self
  end

  def to_s
    first_row = "  0 1 2\n"
    rows = @cells.map.with_index { |row, index| [index, row].flatten.join(CELL_SEPARATOR) }
      .map { |row| "#{row}#{CELL_SEPARATOR}"}

    "#{first_row}#{rows.join("\n")}\n"
  end

  private

  def supported?(sign)
    signs.include?(sign)
  end

  def find_winner_at_cell(x, y)
    winning_combo = find_cell_combos(x, y).find { |cell_combo| check_all_cells_have_same_value(cell_combo) }

    return @cells[x][y] if winning_combo
    NO_WINNER
  end

  def find_cell_combos(x, y)
    [
      [[x, y], [x, y + 1], [x, y + 2]].select { |cell_position| valid_cell_position?(*cell_position) },
      [[x, y], [x + 1, y], [x + 2, y]].select { |cell_position| valid_cell_position?(*cell_position) },
      [[x, y], [x - 1, y + 1], [x - 2, y + 2]].select { |cell_position| valid_cell_position?(*cell_position) },
      [[x, y], [x - 1, y + 1], [x - 2, y + 2]].select { |cell_position| valid_cell_position?(*cell_position) }
    ].select { |combo| combo.size == WINNING_COUNT }
  end

  def check_all_cells_have_same_value(cell_combo)
    cell_combo.map { |x, y| @cells[x][y] }.uniq.size == 1
  end
end

