require 'rake/testtask'

Rake::TestTask.new do |t|
  t.libs << File.expand_path('../../app', __FILE__)
  t.test_files = FileList['test/**/test*.rb']
  t.warning = true
  t.verbose = true
end
