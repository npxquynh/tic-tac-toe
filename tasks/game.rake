# frozen_string_literal: true

require_relative '../app/app'

namespace :game do
  desc 'Start Tic Tac Toe game'
  task :start do
    Game.new.start
  end
end
